package handler

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/redis/go-redis/v9"
	"os"
	"strconv"
	"time"
)

type DataRequestDTO struct {
	Username string `json:"username"`
	DataId   string `json:"data_id"`
	SIZE     string `json:"size"`
}

type RedisRateLimiter struct {
	redisDriver *redis.Client
	MaxTime     time.Duration
	DataRequest DataRequestDTO
}
type RateLimiter interface {
	Validator(message []byte) (DataRequestDTO, error)
}

func NewRateLimiter(driver string) RateLimiter {

	return RedisRateLimiter{redisDriver: redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})}

}
func (receiver RedisRateLimiter) Validator(message []byte) (DataRequestDTO, error) {
	json.Unmarshal(message, &receiver.DataRequest)
	err := receiver.checkWeight()
	if err != nil {
		return DataRequestDTO{}, err
	}
	exists := receiver.checkExist()
	if exists == false {
		receiver.insertData()
		receiver.calculateWeight()
		return receiver.DataRequest, nil
	}

	return receiver.DataRequest, errors.New("already exists")
}

func (receiver RedisRateLimiter) checkExist() bool {
	var ctx = context.Background()
	result, _ := receiver.redisDriver.HGet(ctx, receiver.DataRequest.Username, receiver.DataRequest.DataId).Result()
	if result == "" {

		return false
	}

	return true
}

type data struct {
	DataId string `json:"data_id"`
	SIZE   string `json:"size"`
}

func (receiver RedisRateLimiter) calculateWeight() {

	ctx := context.Background()

	size, err := strconv.Atoi(receiver.DataRequest.SIZE)
	if err != nil {
		return
	}
	receiver.redisDriver.DecrBy(ctx, "weight"+receiver.DataRequest.Username, int64(size))

}
func (receiver RedisRateLimiter) insertData() {
	var ctx = context.Background()
	receiver.redisDriver.HSetNX(ctx, receiver.DataRequest.Username, receiver.DataRequest.DataId, receiver.DataRequest.SIZE)
}

func (receiver RedisRateLimiter) checkWeight() error {
	ctx := context.Background()
	weightRemaining := receiver.redisDriver.Get(ctx, "weight"+receiver.DataRequest.Username).Val()
	if weightRemaining == "" {
		receiver.redisDriver.Set(ctx, "weight"+receiver.DataRequest.Username, os.Getenv("weight"), 0).Val()
		weightRemaining = os.Getenv("weight")
	}
	weightRemainingInt, _ := strconv.Atoi(weightRemaining)
	weightFile, _ := strconv.Atoi(receiver.DataRequest.SIZE)
	if weightRemainingInt <= weightFile {
		return errors.New("cant add file overloaded ")
	}
	return nil

}
