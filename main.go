package main

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"main/handler"
	"os"
)

func main() {
	err := os.Setenv("weight", "50")
	if err != nil {
		panic(err)
	}
	app := fiber.New()

	app.Post("/", func(c *fiber.Ctx) error {
		RateLimiter := handler.NewRateLimiter("redis")
		validator, err := RateLimiter.Validator(c.BodyRaw())
		if err != nil {
			return err
		}
		fmt.Println(validator)
		return nil
	})

	app.Listen(":3000")

}
